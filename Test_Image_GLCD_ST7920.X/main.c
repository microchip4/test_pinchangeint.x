#include <xc.h>
#include "Alteri.h"
#include "mh.h"

//http://www.microchip.com/forums/m830183.aspx
#define DATA_OUT    LATB
#define RW          LATDbits.LATD0
#define RS          LATDbits.LATD1
#define EN          LATDbits.LATD2
#define RST         LATDbits.LATD3

/*
 * Vss      GND
 * Vdd      Vcc (5v)
 * V0       Contrast (Pot)
 * RS       PIN Assigned (Originally LATD1)
 * RW       PIN Assigned (Originally LATD0)
 * E        PIN Assigned (Originally LATD2)
 * D0~D7    PINs Assigned (Originally LAB0~LATB7)
 * PSB      Vcc (5v For parallel Mode) 
 * NC       NC
 * RST      PIN Assigned (Originally LATD3)
 * Vout     NC
 * BLA      Vcc (5v)
 * BLK      GND (330R)
 */

void Send_Data(unsigned char data);
void Send_Command(unsigned char command);
void Set_GraphicMode();
void Initialize();
void Clear_Graphics();
void Fill_Graphics(char Filler);
void Image_Graphics(char * image);
void Write_String ( unsigned Y ,char * string );

char message[16] =  "Hello World!";

void main(void){
    ADCON0=0x00; //all digital
    ADCON1=0x00; //all digital
    ADCON1bits.PCFG=0x0F;
    //__delay_ms(100);  // Allow time for Vdc to settle on GLCD
    TRISB = 0x00;       //Set data port  B as output
    TRISD = 0x00;       // Set control port D as output
    LATB=0;             // port B =0x00
    LATD=0;             // port D=0x00
    RST = 0;            // Set reset line to low
    Initialize();       //Initialise GLCD
    Send_Command(0x80); // Setting location to write characters. In this case 0,0 - Top Left Corner
    Send_Data(0x03);    // Sending a PREDEFINED ? character as described in ST7920 Datasheet.
    Send_Data(0x04);    // another one ?
    Send_Data(0x05);    // another one ?
    Send_Data(0x06);    // and another one. ?
    Write_String(0x90, message);  
    // Calling a function to send a String of characters, as defined in 'message' string.
    // In Extended mode there are four line to print your text. Each line is 16 characters long.
    // Line 1 starts at 0x80 , line 2 starts at 0x90, line 3 starts at 0x88, line 4 starts at 0x99.
    delay_ms(500);
    Initialize();       //Clears all Screen    
    Set_GraphicMode();  // Set the display in Extended mode
    Clear_Graphics();   // Must send a Clear command otherwise display could be corrupt.
    //int Filler = 0;
    while(1){
        //Fill_Graphics(Filler);
        //Filler++;
        Image_Graphics(MH);
        delay_ms(500);
    }
    return ;
}
 //======================== All the command codes below can be found on the ST7920 datasheet ======================
void Initialize(){
    //__delay_ms(100);
    RS=0;
    RW=0;
    __delay_us(400);
    RST = 1;
    __delay_ms(10);             // Short delay after resetting.
    Send_Command(0b00110000) ;  // 8-bit mode.
    __delay_us(100);
    Send_Command(0b00110000) ;  // 8-bit mode again.
    __delay_us(110);
    Send_Command(0b00001100);   // display on
    __delay_us(100);
    Send_Command(0b00000001);   // Clears screen.
    __delay_ms(2);
    Send_Command(0b00000110);   // Cursor moves right, no display shift.
    __delay_us(80);
    Send_Command(0b00000010);   // Returns to home. Cursor moves to starting point.
}

 //========= Setting the control lines to send a Command to the data bus ================
void Send_Command(unsigned char command){
    RW=0;
    RS = 0;
    __delay_us(20);
    EN = 1;
    DATA_OUT = command;
    __delay_us(50);
     EN = 0;
}

//============= Setting the control lines to send Data to the data bus =====================
void Send_Data(unsigned char data){
    RS = 1;
    __delay_us(40);
    DATA_OUT = data;
    __delay_us(30);
    EN = 1;
    __delay_us(20);
    EN = 0;
    __delay_us(20);
}

 //======================= Sent Command to set Extanded mode ====================
void Set_GraphicMode(){
   Send_Command(0b00110100); // Extended instuction set, 8bit
   __delay_us(100);
   Send_Command(0b00110110); // Repeat instrution with bit1 set
   __delay_us(100);
}

//=========== This function set all the pixels to off in the graphic controller =================
void Clear_Graphics(){
    unsigned char x, y;
    for(y = 0; y < 64; y++){
        if(y < 32){
          Send_Command(0x80 | y);
          Send_Command(0x80);
        }
        else{
          Send_Command(0x80 | (y-32));
          Send_Command(0x88);
        }
        for(x = 0; x < 16; x++){
          Send_Data(0x00);
        }
    }
}
//=========== This function set all the pixels to a value in the graphic controller =================
void Fill_Graphics(char Filler){
    unsigned char x, y;
    for(y = 0; y < 64; y++){
        if(y < 32){
            Send_Command(0x80 | y);
            Send_Command(0x80);
        }
        else{
            Send_Command(0x80 | (y-32));
            Send_Command(0x88);
        }
        for(x = 0; x < 16; x++){
            Send_Data(Filler);
        }
    }
}
//=========== This function set all the pixels to off in the graphic controller =================
void Image_Graphics(char * image){
    unsigned char x, y;
    for(y = 0; y < 64; y++){
        if(y < 32){
          Send_Command(0x80 | y);
          Send_Command(0x80);
        }
        else{
          Send_Command(0x80 | (y-32));
          Send_Command(0x88);
        }
        for(x = 0; x < 16; x++){
          Send_Data(*image++);
        }
    }
}
//==== Send one character at the time from the 'message' string ===========
void Write_String ( unsigned Y ,char * string ){
    Send_Command(Y);
    while(*string!= '\0'){      // Looking for code signigying 'end of line' .
        Send_Data(*string++);
    }
}