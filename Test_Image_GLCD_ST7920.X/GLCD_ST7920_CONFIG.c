/*
 * File:   GLCD_ST7920_CONFIG.c
 * Author: Talla
 *
 * Created on 23. Dezember 2016, 16:40
 */


#include <xc.h>
#include"GLCD_ST7920.h"
#include <pic18f1320.h>
#include<stdio.h>
#include<stdlib.h>
#include<htc.h>
#include<string.h>

//Variabeln
        


// PROGRAMME
void INITIALISIERUNG (void){
    IRCF0 = 1 ;             // internen Takt auf direkte 8 MHz setzen
    IRCF1 = 1 ;             // internen Takt auf direkte 8 MHz setzen
    IRCF2 = 1 ;             // internen Takt auf direkte 8 MHz setzen 
     while (! IOFS) {       // warten bis 8MHz stabil sind
        }
    ADCON1 = 0x7F;          // alle Analogeing�nge aus
    TRISB = 0b00000000;     // PortB alles Ausg�nge
    PORTB = 0b00000000;     // initialstate 0
    //LATB2 = 1;  
    __delay_ms(1000);
    LATB3 = 0; //clock
    LATB0 = 0; //clock
    LCD_AUSGABE(LCD_KOMMANDO,LCD_B_FUNKTION);  //     
    LCD_AUSGABE(LCD_KOMMANDO,LCD_LEER);        //
    LCD_AUSGABE(LCD_KOMMANDO,0x06);            //  
    LCD_AUSGABE(LCD_KOMMANDO,0x0C);            //  
}
void LCD_AUSGABE( unsigned char DATEN_KOMANNDO , unsigned char DATEN ){    
    unsigned char temp;   
    if(DATEN_KOMANNDO)   
    {   
        temp = 0xFA;    //RS = 1
    }   
    else    
    {   
        temp = 0xF8;    //RS = 0
    }     
    LCD_SENDEN(temp);   
    temp = DATEN&0xF0;   
    LCD_SENDEN(temp);   
    temp = (DATEN<<4)&0xF0;   
    LCD_SENDEN(temp);     
}
void LCD_SENDEN(unsigned char DATENPUFFER){
    unsigned char k;
    for (k = 8;k>0;k-- ){         
            LATB3 = 1; //Takt an
            LATB0 = (DATENPUFFER & (1 << k-1)) >> k-1; // Daten einzeln rausschaufeln 
            __delay_us(1); //Wenn n�tig muss eine Vez�gerung rein, damit sich niemand verschluckt.
            LATB3 = 0; //Takt aus      
    }
}
void LCD_TEXT(unsigned char x,unsigned char y,const char *DATEN){   
    unsigned char LAENGE,ADRESSE,Z1; 
    y &= 0x03;  //y < 4   
    x &= 0x0F;  //x < 16   
    switch( y )   
    {   
        case 0:   
            ADRESSE = 0x80;   
        break;   
   
        case 1:   
            ADRESSE = 0x90;   
        break;   
   
        case 2:   
            ADRESSE = 0x88;   
        break;   
   
        case 3:   
            ADRESSE = 0x98;   
        break;   
    }   
    ADRESSE += x/2;   
    LAENGE = strlen(DATEN);
    if(LAENGE > 16){            //Textl�nge auf 16 Zeichen begrenzen!
        LAENGE = 16;    
    }
    LCD_AUSGABE(LCD_KOMMANDO,LCD_B_FUNKTION);                
    LCD_DDRAM_ADRESSE(ADRESSE);   
    for(Z1=0;Z1<LAENGE;LAENGE -- )   
        LCD_AUSGABE(LCD_DATEN, *(DATEN++));  
        LCD_AUSGABE(LCD_KOMMANDO,LCD_NEU);
}
void LCD_DDRAM_ADRESSE(unsigned char DDRAM_ADRESSE) {   
    LCD_AUSGABE(LCD_KOMMANDO,LCD_B_FUNKTION);            //  
    LCD_AUSGABE(LCD_KOMMANDO,DDRAM_ADRESSE);             //  
}
void LCD_GR(void){   
     LCD_AUSGABE(LCD_KOMMANDO,LCD_GRAFIKMODUS_AN);
     LCD_AUSGABE(LCD_KOMMANDO,LCD_GRAFIKMODUS_AUS);
}